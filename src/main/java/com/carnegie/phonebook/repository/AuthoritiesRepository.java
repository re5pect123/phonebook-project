package com.carnegie.phonebook.repository;

import com.carnegie.phonebook.entity.Authorities;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthoritiesRepository extends CrudRepository<Authorities, Long> {
    Authorities findByUsername(String username);
}
