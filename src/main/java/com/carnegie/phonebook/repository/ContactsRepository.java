package com.carnegie.phonebook.repository;

import com.carnegie.phonebook.entity.Contacts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContactsRepository extends CrudRepository<Contacts, Long> {
    List<Contacts> findAllByUserId(Integer id);

    Contacts findContactsById(Long id);
}
