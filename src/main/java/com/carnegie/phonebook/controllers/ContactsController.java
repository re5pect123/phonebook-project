package com.carnegie.phonebook.controllers;

import com.carnegie.phonebook.dto.ContactsDto;
import com.carnegie.phonebook.entity.Contacts;
import com.carnegie.phonebook.entity.Users;
import com.carnegie.phonebook.model.Data;
import com.carnegie.phonebook.repository.ContactsRepository;
import com.carnegie.phonebook.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@RestController
public class ContactsController {

    private final static Logger LOGGER = Logger.getLogger(ContactsController.class.getName());

    @Autowired
    ContactsRepository contactsRepository;

    @Autowired
    UsersRepository usersRepository;

    @CrossOrigin
    @GetMapping("listcontacts")
    public Data listOfContacts(Principal principal) {
        LOGGER.info("Received request: /listContacts");
        Users userFromDb = usersRepository.findByUsername(principal.getName());
        LOGGER.info("Info about principal: " + principal.getName());
        List<Contacts> contactsList = contactsRepository.findAllByUserId(userFromDb.getId());
        Data dataTableJS =new Data();
        for (int i = 0; i < contactsList.size(); i++) {
            dataTableJS.data.add(contactsList.get(i));
        }

        return dataTableJS;
    }

    @CrossOrigin
    @PostMapping("addcontacts")
    public String addNewContacts(@RequestBody ContactsDto contactsDto, Principal principal) {
        LOGGER.info("Received request: /addcontacts");
        Users userFromDb = usersRepository.findByUsername(principal.getName());
        Contacts contact = new Contacts();
        contact.setName(contactsDto.getName());
        contact.setNumber(contactsDto.getNumber());
        contact.setUserId(userFromDb.getId());

        contactsRepository.save(contact);
        return "Success save new Contact in DB";
    }

    @CrossOrigin
    @PutMapping("editcontacts/{id}")
    public String editContacts(@PathVariable("id") int id, @RequestBody Contacts contact) {
        LOGGER.info("ID for change = " + id);
        LOGGER.info(contact.toString());

        try {
            Contacts oldContact = contactsRepository.findContactsById(Long.valueOf(id));
            oldContact.setNumber(contact.getNumber());
            oldContact.setName(contact.getName());

            contactsRepository.save(oldContact);
        } catch (Exception e) {
            LOGGER.warning("CHECK ID");
            return "Check id please";
        }

        return "Success edit Contact";
    }

    @CrossOrigin
    @DeleteMapping(value = "deletecontacts/{itemId}")
    public String deleteContacts(@PathVariable int itemId) {
        LOGGER.info("ID for delete = " + itemId);
        try {
            Contacts deleteContact = contactsRepository.findContactsById(Long.valueOf(itemId));
            contactsRepository.delete(deleteContact);
        } catch (Exception e) {
            LOGGER.warning("CHECK ITEM ID");
            return "Check item id please";
        }

        return "Success delete Contact";
    }


}
