package com.carnegie.phonebook.controllers;

import com.carnegie.phonebook.dto.RegisterUserDto;
import com.carnegie.phonebook.entity.Authorities;
import com.carnegie.phonebook.entity.Users;
import com.carnegie.phonebook.msg.Messages;
import com.carnegie.phonebook.repository.AuthoritiesRepository;
import com.carnegie.phonebook.repository.UsersRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.logging.Logger;

@RestController
public class AuthController {

    private final static Logger LOGGER = Logger.getLogger(AuthController.class.getName());
    private final String ROLE = "ROLE_USER";

    @Autowired
    AuthoritiesRepository authoritiesRepository;

    @Autowired
    UsersRepository usersRepository;

    @CrossOrigin
    @PostMapping("register")
    public String register(@RequestBody RegisterUserDto registerUserDto){
        LOGGER.info("Received request: /register");
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        Users user = new Users();
        user.setUsername(registerUserDto.getUsername());
        user.setPassword(bCryptPasswordEncoder.encode(registerUserDto.getPassword()));
        user.setEmail(registerUserDto.getEmail());
        user.setFirstName(registerUserDto.getFirstName());
        user.setLastName(registerUserDto.getLastName());
        user.setEnabled(true);
        usersRepository.save(user);
        LOGGER.info(user.toString());

        Authorities authorities = new Authorities();
        authorities.setUsername(registerUserDto.getUsername());
        authorities.setAuthority(ROLE);
        authoritiesRepository.save(authorities);

        return "Succesful register user";
    }

    @CrossOrigin
    @GetMapping("login")
    public String korisnik(Principal principal) throws NotFoundException {
        LOGGER.info("Received request: /login");
        Authorities a = authoritiesRepository.findByUsername(principal.getName());
        System.out.println("Succesful login = USER: " + principal.getName() + " ROLA = " + a.getAuthority() + "\n");
        if (principal.getName() == null || principal.getName().isEmpty()) {
            throw new NotFoundException(Messages.CHECK_CREDENTIALS);
        }
        return a.getAuthority();
    }


}
