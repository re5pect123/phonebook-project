package com.carnegie.phonebook.controllers;

import com.carnegie.phonebook.entity.Authorities;
import com.carnegie.phonebook.entity.Users;
import com.carnegie.phonebook.repository.AuthoritiesRepository;
import com.carnegie.phonebook.repository.UsersRepository;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.logging.Logger;

@RestController
public class TestController {
    //TODO add dev and prod spring boot profile

    private final static Logger LOGGER = Logger.getLogger(TestController.class.getName());
    private final String ROLE = "ROLE_USER";

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    AuthoritiesRepository authoritiesRepository;

    @CrossOrigin
    @GetMapping("test")
    @ApiOperation(
            value = "Test first API",
            notes = "Test API and check logger and lombok",
            response = TestController.class
    )
    public String test(){

        Users user = new Users();
        user.setUsername("marko");
        user.setPassword("123");
        user.setEmail("marko@hotmail.com");
        user.setFirstName("Marko");
        user.setLastName("Uljarevic");

        LOGGER.info(user.toString());

    return "succesful test";
    }

    @CrossOrigin
    @GetMapping("testdb")
    public String testDb(){

        Users userDb = new Users();
        userDb.setUsername("marko");
        userDb.setPassword("123");
        userDb.setEmail("marko@hotmail.com");
        userDb.setFirstName("Marko");
        userDb.setLastName("Uljarevic");

        usersRepository.save(userDb);

        LOGGER.info(userDb.toString());

        Authorities authorities = new Authorities();
        authorities.setUsername("marko");
        authorities.setAuthority(ROLE);
        authoritiesRepository.save(authorities);

        return "succesful test database";
    }

}
