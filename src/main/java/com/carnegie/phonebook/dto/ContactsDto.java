package com.carnegie.phonebook.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class ContactsDto implements Serializable {

    private String name;
    private String number;
}
