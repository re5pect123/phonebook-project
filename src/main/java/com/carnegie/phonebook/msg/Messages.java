package com.carnegie.phonebook.msg;

public class Messages {

    public static final String BAD_AUTH = "Please check useraname or password";
    public static final String CHECK_API = "API IS OK";
    public static final String CHECK_CREDENTIALS = "Please check credentials";

}
